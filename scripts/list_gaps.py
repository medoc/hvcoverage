"""
Get list of gaps for each data source and save it to CSV file
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os


import hvcoverage

outdir = "output"

if __name__ == "__main__":
    if not os.path.isdir(outdir):
        os.mkdir(outdir)
    ds = hvcoverage.get_datasources()
    for source_id, row in ds.iterrows():
        source_name = row["name"]
        source_name_ = source_name.replace(" ", "_")
        if True:  # True: skip already done, False: overwrite
            if os.path.exists("{}/gaps-{}.csv".format(outdir, source_name_)):
                continue

        print(source_id, source_name, hvcoverage.min_gap(source_name))
        df = hvcoverage.get_data(source_id)

        if True:  # Show histogram of time differences
            gaps = hvcoverage.get_gaps(df, source_name, pd.Timedelta(0, "s"))
            gaps_sec = gaps.apply(
                lambda x: np.log10(x["delta"].total_seconds()), axis=1
            )
            gaps_sec.plot.hist(bins=100, title=source_name, logy=True)
            plt.xlabel("Time delay (log10 [s])")
            plt.savefig("{}/histogram-{}.pdf".format(outdir, source_name_))

        gaps = hvcoverage.get_gaps(df, source_name, hvcoverage.min_gap(source_name))
        gaps.to_csv(
            "{}/gaps-{}.csv".format(outdir, source_name_),
            columns=["time_beg", "time_end", "delta"],
            index=False,
            date_format="%Y-%m-%dT%H:%M:%SZ",
        )  # assumes UTC
