"""
Get list of days containing gaps for any data source
"""

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import os


import hvcoverage

outdir = "output"

if __name__ == "__main__":
    if not os.path.isdir(outdir):
        os.mkdir(outdir)
    ds = hvcoverage.get_datasources()
    union_days = pd.Series()
    for source_id, row in ds.iterrows():
        source_name = row["name"]
        source_name_ = source_name.replace(" ", "_")
        min_gap = hvcoverage.min_gap(source_name)
        print(source_id, source_name, min_gap)
        csvname = "{}/gaps-{}.csv".format(outdir, source_name_)
        if os.path.exists(csvname):
            gaps = pd.read_csv(csvname)
        else:
            df = hvcoverage.get_data(source_id)
            gaps = hvcoverage.get_gaps(df, source_name, min_gap)
        gaps.drop("delta", axis=1)

        for row in gaps.itertuples():
            list_days = pd.date_range(
                pd.Timestamp(row.time_beg).floor("D"),
                pd.Timestamp(row.time_end).floor("D"),
                freq="1D",
            )
            union_days = union_days.append(pd.Series(list_days), ignore_index=True)

    union_days = sorted(union_days.unique())
    union_days = pd.DataFrame(union_days, columns=["day"])
    union_days["next"] = union_days.apply(
        lambda row: row.day + pd.offsets.Day(), axis=1
    )

    union_days.to_csv(
        "{}/uniondays.csv".format(outdir),
        columns=["day", "next"],
        index=False,
        date_format="%Y-%m-%dT%H:%M:%SZ",
    )  # assumes UTC
