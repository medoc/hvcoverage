import hvcoverage

outdir = "output"

ds = hvcoverage.get_datasources()
for ds_id, row in ds.iterrows():
    ds_name = row["name"]
    print(ds_id, ds_name)
    df = hvcoverage.get_data(ds_id)
    hvcoverage.plot_presence(df, ds_name, ["pdf"], outputdir=outdir)
