#!/usr/bin/env python3

"""Plot HelioViewer data coverage at MEDOC, as a function
of time, by querying the database"""

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.colors as colors
import numpy as np
import pandas as pd
from datetime import datetime
import sqlalchemy as sq
import os.path

from . import config


def humansize(size):
    """Human-readable representation of large sizes with SI unit prefixes

    Parameters
    ----------
    size: int
        Number of units

    Return
    ------
    string
        Human-readable representation of number
    """
    if size == 0:
        return "0"
    ls = int(np.log10(size) / 3)
    try:
        prefix = ["", "k", "M", "G", "T", "P", "E"][ls]
    except IndexError:
        raise RuntimeError(
            "Value too small or too large for list of implemented prefixes"
        )
    size /= 1000.0**ls
    return "{:.3g}".format(size) + prefix


def min_gap(source_name):
    """
    Minimum duration considered as a gap, for given source name

    Parameters
    ----------
    source_name: string
        Source name (as in HelioViewer database)

    Return
    ------
    pandas.Timedelta:
        Minimum duration considered as a gap
    """
    instr_name = source_name.split(" ")[0]
    # Lists of (list of sources/instruments, minimum gap duration)
    mg_source = [
        (["AIA 4500"], "3 h"),
    ]
    mg_instr = [
        (
            [
                "EIT",
                "LASCO",
                "COR1-A",
                "COR1-B",
                "COR2-A",
                "COR2-B",
                "EUVI-A",
                "EUVI-B",
                "SXT",
            ],
            "24 h",
        ),
        (["AIA"], "10 m"),  # many smaller gaps...
        (["HMI"], "20 m"),
        (["MDI"], "12 h"),
        (["SWAP"], "1 h"),
    ]

    # Get minimum gap size, first according to full source name,
    # then according to instrument only
    def mg2td(mg):
        s = mg.split(" ")
        return pd.Timedelta(float(s[0]), s[1])

    for source_list, mg in mg_source:
        if source_name in source_list:
            return mg2td(mg)
    for instr_list, mg in mg_instr:
        if instr_name in instr_list:
            return mg2td(mg)


def get_datasources():
    """
    Get list of data sources on HelioViewer server

    Return
    ------
    pandas.dataFrame
        Data sources (index=id, name)
    """
    query = "SELECT id,name FROM datasources WHERE enabled=1"
    engine = sq.create_engine(
        "mysql+mysqlconnector://{}:{}@{}/{}".format(
            config.db_user, config.db_password, config.db_host, config.db_name
        )
    )
    ds = pd.read_sql(query, engine)
    ds.set_index("id", inplace=True)
    return ds


def get_data(source_id, d1=None, d2=None):
    """
    Get metadata for HelioViewer data source

    Parameters
    ----------
    source_id: int
        HelioViewer data source ID
    d1: datetime
        Start date
    d2: datetime
        End date
    Return
    ------
    pandas.DataFrame
        Metadata (index=date, no other column)
    """
    # Build query
    qd1 = "" if d1 is None else " and date > '{}'".format(d1.strftime("%Y-%m-%d"))
    qd2 = "" if d2 is None else " and date < '{}'".format(d2.strftime("%Y-%m-%d"))
    query = "SELECT date FROM data WHERE sourceId='{}'{}{} ;".format(
        source_id, qd1, qd2
    )

    # Run query
    print("Run query: {}".format(query))
    engine = sq.create_engine(
        "mysql+mysqlconnector://{}:{}@{}/{}".format(
            config.db_user, config.db_password, config.db_host, config.db_name
        )
    )
    df = pd.read_sql(query, engine)

    # Use correct timezones
    df.date = df.date.dt.tz_localize("UTC")
    df.set_index("date", inplace=True)

    return df


def plot_presence(df, source_name, formats, d1=None, d2=None, outputdir=""):
    """Plot HelioViewer data coverage

    Parameters
    ----------
    df: pandas.DataFrame
        Metadata (dataframe index is date)
    source_name: string
        HelioViewer data source name
    formats: list of strings
        File formats for plot files
    d1: pandas.datatime
        Start of time interval for plotting
    d2: pandas.datetime
        End of time interval for plotting
    outputdir: string
        Output directory
    """
    # Time interval for plotting
    if d1 is None:
        d1 = df.index.min()
        d1 = datetime(d1.year, d1.month, 1)
    if d2 is None:
        d2 = df.index.max()
        d2 = datetime(d2.year, d2.month, 1) + pd.offsets.MonthBegin(1)

    # Get number of files present at each date
    df["present"] = 1
    dfm = df.resample("1D").sum()
    npres = len(df.index)

    # Simple plot of present number as a function of date
    # dfm.plot()

    # Create image, with axes:
    # * y: month, from d1 to d2
    # * x: day in month, from 1 to 31 (index: day - 1)
    ny = (d2.year - d1.year) * 12 + d2.month - d1.month
    image = np.zeros((ny, 31))
    for day in dfm.index:
        ix = day.day - 1
        iy = (day.year - d1.year) * 12 + day.month - d1.month
        if iy < 0 or iy >= ny:
            continue
        image[iy, ix] = dfm.present[day]

    # Plot
    fig = plt.figure(figsize=(6.4, 5.5))
    ax = fig.gca()
    plot = ax.imshow(
        image,
        aspect="auto",
        interpolation="none",
        origin="lower",
        extent=[0.5, 31.5, mdates.date2num(d1), mdates.date2num(d2)],
        norm=colors.LogNorm(vmin=1),
    )
    ax.xaxis.set_minor_locator(matplotlib.ticker.MultipleLocator(base=1))
    ax.yaxis.set_major_formatter(mdates.DateFormatter("%Y"))
    ax.yaxis.set_major_locator(mdates.YearLocator())
    ax.yaxis.set_minor_locator(mdates.MonthLocator())
    ax.set_xlabel("Day of month")
    ax.set_ylabel("Year")
    ax.set_title("{} in HelioViewer: {} files".format(source_name, humansize(npres)))
    ax.grid(True)
    fig.colorbar(plot)

    for f in formats:
        fig.savefig(
            os.path.join(
                outputdir, "presence-{}.{}".format(source_name.replace(" ", "_"), f)
            )
        )
    fig.clear()


def get_gaps(df, source_name, min_gap_size, d1=None, d2=None):
    """Plot get gaps in data coverage

    Parameters
    ----------
    df: pandas.DataFrame
        Metadata (dataframe index is date)
    source_name: string
        HelioViewer data source name
    min_gap_size:
        Minimum gap size for defining a gap
    d1: pandas.datatime
        Start of time interval for plotting
    d2: pandas.datetime
        End of time interval for plotting

    Return
    ------
    pandas.DataFrame:
        List of gaps: begin date, end date, gap duration.
    """
    # Time interval to consider
    if d1 is None:
        d1 = df.index.min()
    if d2 is None:
        d2 = df.index.max()
    df["time_beg"] = df.index  # .shift() does not work on .index
    df["time_end"] = df.time_beg.shift(-1)
    df["delta"] = df.time_end - df.time_beg
    df_gap = df[df.delta > min_gap_size]
    return df_gap
