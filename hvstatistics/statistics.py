#!/usr/bin/env python3

'''Plot HelioViewer access statistics, as a function
of time, by querying the database'''

import matplotlib.pyplot as plt
import pandas as pd
import sqlalchemy as sq

from . import config


def get_data(d1=None, d2=None):
    '''
    Get list of access to HelioViewer data by action type

    Parameters
    ----------
    d1: datetime or pandas.Timestamp
        Start date
    d2: datetime or pandas.Timestamp
        End date
    Return
    ------
    pandas.DataFrame
        Metadata (index=date, no other column)
    '''
    # Build query
    qd = list()
    if d1 is not None:
        qd.append(f"timestamp > '{d1.strftime('%Y-%m-%d %H:%M:%S')}'")
    if d2 is not None:
        qd.append(f"timestamp < '{d2.strftime('%Y-%m-%d %H:%M:%S')}'")
    qd = ' AND '.join(qd)
    if qd != '':
        qd = ' WHERE ' + qd
    query = f"SELECT timestamp,action FROM statistics{qd} ;"

    # Run query
    print('Run query: {}'.format(query))
    engine = sq.create_engine("mysql+mysqlconnector://{}:{}@{}/{}".format(config.db_user, config.db_password, config.db_host, config.db_name))
    df = pd.read_sql(query, engine)

    # Use correct timezones
    df.timestamp = df.timestamp.dt.tz_localize('UTC')
    df.set_index('timestamp', inplace=True)

    # Add columns for each action
    actions = df.action.unique()
    for action in actions:
        df[action] = 0
        df.loc[df.action == action, action] = 1
    return df



def plot_statistics(df, period='D'):
    '''
    Plot statistics of access to HelioViewer data

    Parameters
    ----------
    df: pandas.DataFrame
        list of access to HelioViewer data by action type
    period: string
        Resampling period (for pandas.DataFrame.resample)
    '''
    dr = df.resample(period).sum().sort_index(axis=1)
    namePeriod = {'H': 'hour', 'D': 'day', 'W': 'week', 'ME': 'month', 'M': 'month', '2ME': '2 months', '2M': '2 months', 'Q': 'quarter', 'Y': 'year'}[period]
    dr.plot(kind='area',
            title=f'Number of access to HelioViewer server per {namePeriod}',
            grid=True)
    plt.show()
    print('Total number of access during period:')
