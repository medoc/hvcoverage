#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
HelioViewer access statistics
Author: Eric Buchlin, eric.buchlin@ias.u-psud.fr

@package hvstatistics
"""

from .statistics import get_data, plot_statistics
