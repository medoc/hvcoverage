# HelioViever coverage

Get data coverage from HelioViewer database.


## Configuration

In `hvcoverage`, copy `config_template.py` to `config.py` and write your HelioViewer database username, password, host, and database name. Be careful to protect your password...


## Quick usage example

To get a list of data sources

```python
import hvcoverage

ds = hvcoverage.get_datasources()
print(ds)
```

To produce a plot of the AIA 171 data coverage (assuming it is data source ID 10 in `ds`)

```python
source_id = 10
df = hvcoverage.get_data(source_id)
hvcoverage.plot_presence(df, ds.name[source_id], ['pdf'])
```

To produce data coverage plots for all "enabled" data source:

```sh
python scripts/plot_all.py
```

To get a CSV file listing gaps, for each data source:
```sh
python scripts/list_gaps.py
```

# HelioViever statistics

## Configuration

In `hvcoverage`, copy `config_template.py` to `config.py` and write your HelioViewer database username, password, host, and database name. Be careful to protect your password...


## Quick usage example

```python
import hvstatistics
import pandas as pd
df = hvstatistics.get_data(d1=pd.Timestamp('2020-01-01'), d2=pd.Timestamp('2024-05-01'))
hvstatistics.plot_statistics(df, period='ME')
```
